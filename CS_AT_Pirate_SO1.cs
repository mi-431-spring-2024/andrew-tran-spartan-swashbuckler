using System;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using XnTools;
using XnTools.XnAI;
using Random = UnityEngine.Random;

namespace AT {
[CreateAssetMenu(fileName = "CS_AT_Pirate_SO1",
menuName = "ScriptableObjects/Captain Strategies/CS_AT_Pirate_SO1", order = 1)]
public class CS_AT_Pirate_SO1 : CaptainStrategy_XnAI_SO
{
    // NOTE: This is assigned by SeaMap for the Merchants, but you can populate it as you travel around - SPH 2023-04-02
    static private List<Vector2Int> _SETTLEMENTS;
    static private List<Vector2Int> _HAVENS;
    static private List<Vector2Int> _UNKNOWN;

    [HorizontalLine()]
    [SerializeField] private bool useDiagonalNavigation = true;
    [SerializeField] private float navPointIsCloseEnough = 10;
    [SerializeField] private int varianceOnStraightLines = 20;


    // My variables
    private float turnValue = 0;
    private Vector3 shipPos;

    [SerializeField]
    private Vector2Int startMapLoc, endMapLoc;
    private List<Vector3> navPath;
    private AStarSeaMapPathfinder pathfinder;
    private bool plotNavPathCalled = false;

    public override void InitCaptain()
    {
        // Clear the old navigation data.
        plotNavPathCalled = false;
        navPath = new List<Vector3>();
        personalLog.Log(this, "InitCaptain Called.");
    }

    protected override void SetCaptainName()
    {
        captainName = "CS_AT_Pirate_SO";
    }

    private SenseInfo _selfInfo;
    private List<SenseInfo> _sensed;

    /// <summary>
    /// <para>This function is called every FixedUpdate, and it is the only way that your Captain
    ///  can give commands to the ship. See CS_SPH_Merchant_2_SO and CS_HumanPlayer_SO for examples.</para>
    /// <para>Ship.eCommands can be found in the Ship script and include:</para>
    /// <para><b>none</b> - Do nothing</para>
    /// <para><b>sailsUp</b> - Raise the sails to Accelerate</para>
    /// <para><b>sailsDown</b> - Drop the sails to Decelerate</para>
    /// <para><b>turnLeft</b> - Turn left, decreasing the heading</para>
    /// <para><b>turnRight</b> - Turn right, increasing the heading</para>
    /// <para><b>turnToHeading, float toHeading</b> � Turn toward toHeading</para>
    /// <para><b>fire</b> - Fire all cannon pairs randomly (only the starboard or port cannon of each pair will fire)</para>
    /// <para><b>fireAtPoint, Vector3 aimPoint</b> - Attempt to aim cannon at a specific point in world space. Cannon aim angle is limited by SETTINGS</para>
    /// </summary>
    public override List<Ship.Command> AIUpdate(SenseInfo selfInfo, List<SenseInfo> sensed)
    {
        // Set up ports, if needed
        if (_UNKNOWN == null)
        {
            _UNKNOWN = new List<Vector2Int>(PublicInfo.MAP_PORTS);
            _SETTLEMENTS = new List<Vector2Int>();
            _HAVENS = new List<Vector2Int>();
        }

        commands.Clear(); // These will also be cleared by the Ship as it attempts them.

        _selfInfo = selfInfo;
        _sensed = sensed;

        aiTree.Execute();

        // Moved searching for path to FindRouteToPort

        // Moved following the path to FollowRouteToPort

        return commands;
    }

    private bool identifyingPorts = false;

    /// <summary>
    /// Find a port that is a significant distance away and plot a path to it using A*
    /// </summary>
    bool NavToNearestUnknownPort()
    {
        if (_UNKNOWN.Count == 0)
        {
            identifyingPorts = false;
            return false;
        }

        // Choose a start and end point
        startMapLoc = _selfInfo.mapLoc;
        int closestDistSq = Int32.MaxValue, distSq;
        int closestIndex = 0;
        Vector2Int delta;
        for (int i = 0; i < _UNKNOWN.Count; i++)
        {
            delta = _UNKNOWN[i] - startMapLoc;
            distSq = delta.x * delta.x + delta.y * delta.y;
            if (distSq < closestDistSq)
            {
                closestDistSq = distSq;
                closestIndex = i;
            }
        }
        endMapLoc = _UNKNOWN[closestIndex];
        personalLog.Log("Nearest unknown port is:", endMapLoc.ToString());
        identifyingPorts = true;
        PlotNavPath(startMapLoc, endMapLoc);
        return true;
    }

    void PlotNavPath(Vector2Int mapLocFrom, Vector2Int mapLocTo)
    {
        personalLog.Log("PathFindCoRo started...");

        pathfinder = new AStarSeaMapPathfinder();
        GameManager.START_COROUTINE(pathfinder.PathFindCoRo(mapLocFrom, mapLocTo, useDiagonalNavigation, PathFound));

        plotNavPathCalled = true;
    }

    void PathFound(List<AStarComparable> foundPath)
    {
        personalLog.Log("PathFound callback", foundPath?.ToStringExpanded());
        pathfinder = null; // Release the memory for pathfinder
        plotNavPathCalled = false; // Ask for another path
        if (foundPath == null || foundPath.Count < 2)
        {
            personalLog.Log("PathFound was null or <2", foundPath?.ToStringExpanded());
            return;
        }

        float[] pathLerpAmounts = new[] { Random.Range(0.35f, 0.45f), 0.5f, Random.Range(0.55f, 0.65f) };
        navPath = new List<Vector3>();
        Vector3 p0, p1, n0, n1, n2, thisDir, lastDir = Vector3.zero;
        float randy = Random.Range(-varianceOnStraightLines, varianceOnStraightLines);
        for (int i = 1; i < foundPath.Count; i++)
        {
            p0 = foundPath[i - 1].position;
            p1 = foundPath[i].position;
            n0 = Vector3.Lerp(p0, p1, pathLerpAmounts[0]);
            n1 = Vector3.Lerp(p0, p1, pathLerpAmounts[1]);
            n2 = Vector3.Lerp(p0, p1, pathLerpAmounts[2]);
            thisDir = (n2 - n0);//.normalized;
            if (n0.x.IsApproximately(n1.x, 1) && n1.x.IsApproximately(n2.x, 1))
            {
                // It's straight in the x dimension, so it can be varied
                n0.x += randy;
                n1.x += randy;
                n2.x += randy;
            }
            else if (n0.z.IsApproximately(n1.z, 1) && n1.z.IsApproximately(n2.z, 1))
            {
                // It's straight in the x dimension, so it can be varied
                n0.z += randy;
                n1.z += randy;
                n2.z += randy;
            }
            else if ((thisDir - lastDir).magnitude < 1f)
            {
                n0.x += randy;
                n1.x += randy;
                n2.x += randy;
                n0.z -= randy;
                n1.z -= randy;
                n2.z -= randy;
            }
            lastDir = thisDir;
            navPath.AddRange(n0, n1, n2);
        }
    }

    private Color col = Color.clear;
    public override void OnDrawGizmos()
    {
        if (navPath != null && navPath.Count > 0)
        {
            if (col == Color.clear)
            {
                col = Random.ColorHSV(0, 1, 1, 1, 1, 1);
            }
            Vector3 offset = Vector3.up * 2;
            Gizmos.color = col;

            Gizmos.DrawLine(_selfInfo.position + offset, navPath[0] + offset);
            for (int i = 1; i < navPath.Count; i++)
            {
                Gizmos.DrawLine(navPath[i - 1] + offset, navPath[i] + offset);
            }

        }
    }



    #region XnAI Methods - See more example methods in CaptainStrategy_XnAI_SO.cs

    #region XnAI Methods - Execute
    // A* Navigation to Destination Port
    public eStatus FindRouteToPort()
    {
        if (navPath != null && navPath.Count > 0) return eStatus.success;
        if (plotNavPathCalled) return eStatus.busy;
        if (NavToNearestUnknownPort())
        {
            return eStatus.busy;
        }
        else
        {
            // If ALL ports are known, finding a route fails.
            return eStatus.failed;
        }
    }

    /// <summary>
    /// Explore around to any random point that is navigable
    /// </summary>
    /// <returns></returns>
    public eStatus FindAnyNavigableTile()
    {
        identifyingPorts = true;
        if (navPath != null && navPath.Count > 0) return eStatus.success;
        if (plotNavPathCalled) return eStatus.busy;

        Vector2Int mapLoc = Vector2Int.zero;
        bool[,] navigable = PublicInfo.NAVIGABLE;
        int size = navigable.GetLength(0);
        do
        {
            mapLoc.x = Random.Range(0, size);
            mapLoc.y = Random.Range(0, size);
        } while (!navigable[mapLoc.x, mapLoc.y]);
        // We should now have a navigable mapLoc. Let's head to it
        startMapLoc = _selfInfo.mapLoc;
        endMapLoc = mapLoc;
        personalLog.Log("Plotting path to random mapLoc:", endMapLoc.ToString());
        PlotNavPath(startMapLoc, endMapLoc);
        return eStatus.busy;
    }


    void ClearNavPath()
    {
        plotNavPathCalled = false;
        navPath.Clear();
    }


    public eStatus FollowRouteToPort()
    {
        // First, find out if we know what kind of port it is
        Vector2Int sensedMapLoc;
        plotNavPathCalled = false;
        if (identifyingPorts)
        {
            for (int i = 0; i < _sensed.Count; i++)
            {

                if (_sensed[i].senseType.HasAnyFlag(eMapSense.port_identified))
                {
                    sensedMapLoc = _sensed[i].mapLoc;
                    // This will catch and identify any unknown port, regardless of if it's endMapLoc
                    int unknownIndex = _UNKNOWN.IndexOf(sensedMapLoc);
                    if (unknownIndex != -1)
                    {
                        if (_sensed[i].senseType.HasFlag(eMapSense.port_haven))
                        {
                            _HAVENS.Add(sensedMapLoc);
                            personalLog.Log($"Learned that mapLoc {sensedMapLoc} is a Pirate Haven!");
                        }
                        else
                        {
                            _SETTLEMENTS.Add(sensedMapLoc);
                            personalLog.Log($"Learned that mapLoc {sensedMapLoc} is a Settlement!");
                        }
                        _UNKNOWN.Remove(sensedMapLoc);
                        if (_UNKNOWN.Count == 0) { personalLog.Log($"I have identified ALL PORTS!"); }
                    }
                    if (_sensed[i].mapLoc == endMapLoc)
                    {
                        personalLog.Log($"Got close enough to see endMapLoc: {endMapLoc}");
                        // We know what kind of port is at endMapLoc!
                        ClearNavPath();
                        // This is actually a success, but we'll return failed so that the composite parent node moves on
                        return eStatus.success;
                    }
                }
            }
        }


        
        // Determine whether we're close enough to move to the next point on the path
        Vector3 nextNavPoint = navPath[0];
        Vector3 deltaToNavPoint = nextNavPoint - _selfInfo.position;
        Vector3 headingDir = PublicInfo.HeadingToDirection(_selfInfo.heading);
        // Deal with the issue where the ship can get stuck circling a navPoint because it can't turn quickly enough
        // If we're facing the wrong way and the navPoint is close, drop sails to turn faster
        float dotToDesiredDir = Vector3.Dot(deltaToNavPoint.normalized, headingDir);
        if (dotToDesiredDir < 0.5f)
        {
            if (deltaToNavPoint.magnitude < 200)
            {
                if (_selfInfo.sailsUp) commands.Add(new Ship.Command(Ship.eCommand.sailsDown));
            }
            else
            {
                if (!_selfInfo.sailsUp) commands.Add(new Ship.Command(Ship.eCommand.sailsUp));
            }
        }
        else
        {
            if (!_selfInfo.sailsUp) commands.Add(new Ship.Command(Ship.eCommand.sailsUp));
        }

        if (navPath.Count > 1)
        {
            // Determine whether we're between navPath[0] and navPath[1]
            Vector3 vecTo1 = navPath[1] - _selfInfo.position;
            // If the dot product of the two is negative, this ship is between the two points
            if (Vector3.Dot(deltaToNavPoint, vecTo1) < 0)
            {
                navPath.RemoveAt(0);
                nextNavPoint = navPath[0];
            }
        }
        if ((nextNavPoint - _selfInfo.position).magnitude <= navPointIsCloseEnough)
        {
            navPath.RemoveAt(0);
            // Are we done?
            if (navPath.Count == 0)
            {
                personalLog.Log("Reached endMapLoc!");
                ClearNavPath();
                return eStatus.failed;
            }
            // Otherwise, head to the new nav point
            nextNavPoint = navPath[0];
        }

        float desiredHeading = SeaMap.DirectionToHeading(deltaToNavPoint);
        commands.Add(new Ship.Command(Ship.eCommand.turnToHeading, desiredHeading));
        return eStatus.busy;
    }

    // Defend in Combat => Head to Nearest Port
    public eStatus IsAbleToHealInPort()
    {
        // Do I need to heal?
        // Has enough time passed since healing last time?
        return eStatus.failed;
    }

    public eStatus IsCloseEnoughToSettlement()
    {

        return eStatus.failed;
    }

    public eStatus RunToPort()
    {
        if (_selfInfo.health <= 50)
        {
            FindRouteToPort();
            FollowRouteToPort();
            return eStatus.success;
        }
        else
        {
            return eStatus.failed;
        }
    }

    // Defend in Combat => Turn and Shoot
    public eStatus AreEnoughCannonLoaded()
    {
        if (_selfInfo.cannon_loaded > 5)
        {
            return eStatus.success;
        }
        else
        {
            return eStatus.failed;
        }
    }

    public eStatus IsEnemyInRange()
    {
        for (int i = 0; i < _sensed.Count; i++)
        {
            if (_sensed[i].senseType.HasAnyFlag(eMapSense.ship) && _sensed[i].shipID != _selfInfo.shipID)
            {
                shipPos = _sensed[i].position;
                if (Math.Abs(shipPos.x - _selfInfo.position.x) < 200 && Math.Abs(shipPos.y - _selfInfo.position.y) < 200 && Math.Abs(shipPos.z - _selfInfo.position.z) < 200)
                {
                    Fire();
                    personalLog.Log("Found enemy!");
                    ClearNavPath();

                    navPath.Add(_sensed[i].position );
                    float desiredHeading = SeaMap.DirectionToHeading(_sensed[i].position - _selfInfo.position);
                    if (Mathf.Abs(desiredHeading - _selfInfo.heading) >= 10 && Mathf.Abs(desiredHeading - _selfInfo.heading) <= 358)
                    {
                        commands.Add(new Ship.Command(Ship.eCommand.turnToHeading, desiredHeading));
                    }

                    if (Math.Abs(shipPos.x - _selfInfo.position.x) < 30 && Math.Abs(shipPos.y - _selfInfo.position.y) < 30 && Math.Abs(shipPos.z - _selfInfo.position.z) < 30)
                    {
                        return eStatus.success;
                    }
                    else
                    {
                        return eStatus.busy;
                    }


                }
                else
                {
                    personalLog.Log("Lost ship pos");
                    return eStatus.failed;
                }
            }
            else
            {
                //return eStatus.failed;
            }
        }
        return eStatus.busy;
    }

    public eStatus TurnToAim()
    {
        ClearNavPath();
        //commands.Add(new Ship.Command(Ship.eCommand.fireAtPoint, shipPos));
        commands.Add(new Ship.Command(Ship.eCommand.turnRight));
        return eStatus.success;
        //return eStatus.failed;
    }

    public eStatus Fire()
    {
        commands.Add(new Ship.Command(Ship.eCommand.fire, shipPos));
        //commands.Add(new Ship.Command(Ship.eCommand.fire));
        return eStatus.success;
    }

    // Defend in Combat
    public eStatus RunAway()
    {
        FollowRouteToPort();

        return eStatus.failed;
    }

    public eStatus IsEnemyHealthLow()
    {
        personalLog.Log("made it to enemy health check");
        //Put storing of that enemy's GO here 

        //if the ship's health is below 50, chase. If not, return failed
        /*
        for (int i = 0; i < _sensed.Count; i++)
        {
            if (_sensed[i].senseType.HasAnyFlag(eMapSense.ship_merchant) || _sensed[i].senseType.HasAnyFlag(eMapSense.ship_pirate))
            {

            }

        }
        */
        return eStatus.failed;
        
    }

    public eStatus DefenseHealthLow()
    {
        if (_selfInfo.health > 50)
        {
            return eStatus.failed;
        }
        else
        {
            return eStatus.success;
        }
    }
    public eStatus AttackHealthLow()
    {
        if (_selfInfo.health > 50)
        {
            return eStatus.success;
        }
        else
        {
            return eStatus.failed;
        }
    }

    public eStatus IsPlayerWealthHigh()
    {
        
        if (_selfInfo.wealth > 50)
        {
            FollowRouteToPort();
            return eStatus.failed;
        }
        else
        {
            return eStatus.success;
        }        
    }

    public eStatus ReturnFireRandom()
    {
        if (_selfInfo.cannon_loaded > 8)
        {
            commands.Add(new Ship.Command(Ship.eCommand.fire));

        }

        return eStatus.busy;
    }

    public eStatus IsWealthTooHigh()
    {
        if (_selfInfo.wealth > 50)
        {
            return eStatus.success;
        }
        else
        {
            return eStatus.failed;
        }
        //return eStatus.busy;
    }

    #endregion

    #region XnAI Methods - Utility



    #endregion

    #region XnAI Methods - Utility Decorators



    #endregion

    #endregion
}
}
